# ItunesMusicSearcher #

App to search songs on iTunes

### Starting ###

* You can clone the project and run it.

### Next steps... if there is more time ###

* I include some unit tests
* I show more details about each track due Itunes give us more details per track
* I add CI to generate pipelines (through bitbucket) 

### Screenshots ###

![](/screenshots/phone_list.png)

![](/screenshots/phone_details.png)

![](/screenshots/tablet.png)

### Author ###

* **[Jose Antonio Barros](https://www.linkedin.com/in/jbarrosramos/)**