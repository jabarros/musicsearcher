package com.codetest.music.searcher.tasks;

import com.codetest.music.searcher.model.Track;

import java.util.ArrayList;

/**
 * Created by Jose Antonio on 17/07/2017.
 */

public interface GetTracksCallback {
    public void displayParsedTracks(ArrayList<Track> tracks);
}
