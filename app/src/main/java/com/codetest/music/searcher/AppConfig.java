package com.codetest.music.searcher;

/**
 * Created by Jose Antonio on 18/07/2017.
 */

public class AppConfig {

    public static final String API_URL = "https://itunes.apple.com/search?term=";
}
