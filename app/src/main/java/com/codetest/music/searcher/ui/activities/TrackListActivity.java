package com.codetest.music.searcher.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.codetest.music.searcher.AppConfig;
import com.codetest.music.searcher.R;
import com.codetest.music.searcher.model.Track;
import com.codetest.music.searcher.tasks.GetImageTask;
import com.codetest.music.searcher.tasks.GetTracksCallback;
import com.codetest.music.searcher.tasks.GetTracksTask;
import com.codetest.music.searcher.ui.fragments.TrackDetailFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Activity to search and list the track results
 */
public class TrackListActivity extends AppCompatActivity {

    private static final String TAG = TrackListActivity.class.getName();

    private boolean mTwoPane;

    View mRecyclerView;

    EditText searchText;

    ArrayList<Track> tracksList;

    GetTracksCallback tracksCallback = new GetTracksCallback() {
        @Override
        public void displayParsedTracks(ArrayList<Track> tracks) {
            updateResultsView(tracks);
            tracksList = tracks;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        searchText = (EditText) findViewById(R.id.editText);

        mRecyclerView = findViewById(R.id.track_list);
        assert mRecyclerView != null;

        if (findViewById(R.id.track_detail_container) != null) {
            mTwoPane = true;
        }

        //TODO Avoid to lost info when changing device orientation
    }

    public void search(View v) {
        if (isOnline()) {
            GetTracksTask getTracksTask = new GetTracksTask(this, tracksCallback);
            String searchTerm = searchText.getText().toString();
            if (searchTerm.length() > 0) {
                searchTerm = searchTerm.replace(" ", "+");
                getTracksTask.execute(AppConfig.API_URL + searchTerm);
            } else {
                Toast.makeText(this, R.string.no_keyword_given, Toast.LENGTH_SHORT).show();
            }

            // Close keyboard
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(searchText.getWindowToken(), 0);
        } else {
            Toast.makeText(this, R.string.error_no_connection, Toast.LENGTH_LONG).show();
        }
    }

    private void updateResultsView(ArrayList<Track> tracks) {


        for (final Track track : tracks) {
            try {
                String rawDate = track.getReleaseDate().substring(0, 10).trim();
                Date dateFormat = new SimpleDateFormat("yyyy-MM-dd").parse(rawDate);
                String newDate = new SimpleDateFormat("MM-dd-yyyy").format(dateFormat);
                track.setReleaseDate(newDate);
            } catch (Exception e) {
                Log.d(TAG,"Failed to convert date");
            }
        }

        ((RecyclerView) mRecyclerView).setAdapter(new SimpleItemRecyclerViewAdapter(tracks));
    }

    protected boolean isOnline() {
        String cs = Context.CONNECTIVITY_SERVICE;
        ConnectivityManager cm = (ConnectivityManager)getSystemService(cs);
        if (cm.getActiveNetworkInfo() == null) {
            return false;
        } else {
            return true;
        }
    }

    public class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

        private final List<Track> mValues;

        public SimpleItemRecyclerViewAdapter(List<Track> items) {
            mValues = items;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.track_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            holder.mItem = mValues.get(position);
            GetImageTask getImageTask = new GetImageTask(holder.mThumbnailArtWork);
            getImageTask.execute(mValues.get(position).getArtworkUrl100());
            holder.mTrackName.setText(mValues.get(position).getTrackName());
            holder.mArtistName.setText(mValues.get(position).getArtistName());

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mTwoPane) {
                        Bundle arguments = new Bundle();
                        arguments.putSerializable(TrackDetailFragment.ARG_ITEM_ID, holder.mItem);
                        TrackDetailFragment fragment = new TrackDetailFragment();
                        fragment.setArguments(arguments);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.track_detail_container, fragment)
                                .commit();
                    } else {
                        Context context = v.getContext();
                        Intent intent = new Intent(context, TrackDetailActivity.class);
                        intent.putExtra(TrackDetailFragment.ARG_ITEM_ID, holder.mItem);

                        context.startActivity(intent);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final TextView mTrackName;
            public final TextView mArtistName;
            public final ImageView mThumbnailArtWork;
            public Track mItem;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                mTrackName = (TextView) view.findViewById(R.id.trackName);
                mArtistName = (TextView) view.findViewById(R.id.artistName);
                mThumbnailArtWork = (ImageView) view.findViewById(R.id.thumbnailArtWork);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + mTrackName.getText() + "'";
            }
        }
    }
}
