package com.codetest.music.searcher.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.codetest.music.searcher.R;
import com.codetest.music.searcher.model.Track;
import com.codetest.music.searcher.tasks.GetImageTask;
import com.codetest.music.searcher.ui.activities.TrackDetailActivity;
import com.codetest.music.searcher.ui.activities.TrackListActivity;

/**
 * A fragment representing a single Track detail screen.
 * This fragment is either contained in a {@link TrackListActivity}
 * in two-pane mode (on tablets) or a {@link TrackDetailActivity}
 * on handsets.
 */
public class TrackDetailFragment extends Fragment {

    public static final String ARG_ITEM_ID = "item_id";

    private Track mItem;

    public TrackDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {

            mItem = (Track)getArguments().getSerializable(ARG_ITEM_ID);

            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(mItem.getTrackName());
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.track_detail, container, false);

        if (mItem != null) {
            GetImageTask getImageTask = new GetImageTask((ImageView)rootView.findViewById(R.id.trackImage));
            getImageTask.execute(mItem.getArtworkUrl100());

            String albumTextComposed = String.format(getString(R.string.track_album_text), mItem.getCollectionName());
            String priceTextComposed = String.format(getString(R.string.track_price_text), mItem.getTrackPrice());
            String dateTextComposed = String.format(getString(R.string.track_release_text), mItem.getReleaseDate());

            ((TextView) rootView.findViewById(R.id.trackName)).setText(mItem.getTrackName());
            ((TextView) rootView.findViewById(R.id.artistName)).setText(mItem.getArtistName());


            ((TextView) rootView.findViewById(R.id.albumName)).setText(albumTextComposed);
            ((TextView) rootView.findViewById(R.id.trackPrice)).setText(priceTextComposed);
            ((TextView) rootView.findViewById(R.id.trackRelease)).setText(dateTextComposed);
        }

        return rootView;
    }
}
